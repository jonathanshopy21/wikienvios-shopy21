'use strict'

const express = require('express');
const bodyParser = require('body-parser');
const hbs = require('hbs');
// const passport = require('passport');
// const flash = require('connect-flash');
// const session = require('express-session');
const app = express();
var paypal = require('paypal-rest-sdk');
const config = require('./config');

paypal.configure(config.PAYPAL_CONFIG);

// configuracion HBS
app.set('view engine', 'hbs');
hbs.registerPartials(__dirname + '/views/partials');


// Middlewares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// public
app.use(express.static(__dirname + '/public'));

// Rutas
const paypalRoutes = require('./sources/routes/paypal');
const stripeRoutes = require('./sources/routes/stripe');
const usuariosRoutes = require('./sources/routes/usuarios');
const cotizacionRoutes = require('./sources/routes/cotizacion');
const dashboardRoutes = require('./sources/routes/dashboard');

app.use('/', paypalRoutes);
app.use('/', stripeRoutes);
app.use('/', usuariosRoutes);
app.use('/', cotizacionRoutes);
app.use('/', dashboardRoutes);

// Vistas
app.get('/panel', (request, response) => {
    response.render('panel');
});

app.get('/inicio-sesion', (request, response) => {
    response.render('inicio-sesion');
});

app.get('/prueba-modal', (request, response) => {
    response.render('prueba-modal');
});

app.get('/envios', (request, response) => {
    response.render('envios');
});

// Exportar
module.exports = app;
