'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * This model repersents the admin user that has the necessary capacities of editing and manage any information related
 * to the platform an its users. An AdminUser is in charge of upgrading the level status of a client or provider
 * subscription. 
 * @param {Date}    fechaDelPago    Este campo guarda la fecha y hora en que se realizo el pago
 * @param {Number}  monto           Este campo guarda el monto del pago realizado
 * @param {String}  banco           Este campo guarda el nombre del banco del cual proviene el pago
 * @param {String}  formaDePago     Este campo guarda la forma en que se realiza el pago ()
 * @param {String}  referencia      Este campo guarda la referencia del pago realizado
 * @param {String}  estatus         Este campo guarda el estatus del pago realizado ()
 */
const PagoSchema = Schema({
    fechaDelPago: { type: Date, required: true },
    monto: { type: Number, required: true },
    banco: { type: String, required: true },
    formaDePago: { type: String, required: true },
    referencia: { type: String, required: true },
    estatus: { type: String, required: true },
});

module.exports = mongoose.model('Pago', PagoSchema);