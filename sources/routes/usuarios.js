'use strict'

const express = require('express');
const router = express.Router();
const controladorUsuarios = require('../controllers/usuarios');

router.get('/usuarios-test', controladorUsuarios.test);
router.post('/guardar-usuario', controladorUsuarios.guardarUsuario);
router.post('/inicio-sesion', controladorUsuarios.inicioDeSesion);

module.exports = router;