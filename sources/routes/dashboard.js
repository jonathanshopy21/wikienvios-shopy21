'use strict'

const express = require('express');
const router = express.Router();

// VISTAS DEL DASHBOARD
router.get('/dashboard', (request, response) => {
    response.render('panel');
});

router.get('/dashboard/cotizacion', (request, response) => {
    response.render('cotizacion');
});

router.get('/dashboard/envios', (request, response) => {
    response.render('envios');
});

router.get('/dashboard/rastreo', (request, response) => {
    response.render('rastreo');
});

router.get('/dashboard/historial', (request, response) => {
    response.render('historial');
});

router.get('/dashboard/directorio', (request, response) => {
    response.render('directorio');
});

router.get('/dashboard/config', (request, response) => {
    response.render('configuracion');
});

router.get('/enviamos-email', (request, response) => {
    response.render('enviamos-email');
});

router.get('/recover-password', (request, response) => {
    response.render('recuperacion-contraseña');
});

module.exports = router;