'use strict'

const express = require('express');
const router = express.Router();
const StripeController = require('../controllers/stripe');

// METODOS STRIPE
router.post('/comprar-stripe', StripeController.comprarStripe);

// VISTAS DE PRUEBA STRIPE
router.get('/compra-stripe-prueba', (request, response) => {
    response.render('compra-stripe-prueba');
});

router.get('/compra-stripe-exitosa', (request, response) => {
    response.render('compra-stripe-exitosa');
});

module.exports = router;