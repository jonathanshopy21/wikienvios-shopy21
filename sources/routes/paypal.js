'use strict'

const express = require('express');
const router = express.Router();
const PaypalController = require('../controllers/paypal');

// METODOS PAYPAL
router.post('/comprar-paypal', PaypalController.crearCompraPaypal);
router.get('/procesando-paypal', PaypalController.concretarCompra);

// VISTAS DE PRUEBA PAYPAL
router.get('/compra-paypal-prueba', (request, response) => {
    response.render('compra-paypal-prueba');
});

router.get('/compra-paypal-exitosa', (request, response) => {
    response.render('compra-paypal-exitosa');
});

router.get('/compra-paypal-error', (request, response) => {
    response.render('compra-paypal-error');
});


module.exports = router;