'use strict'

const express = require('express');
const router = express.Router();
const controladorCotizacion = require('../controllers/cotizacion');

router.get('/cotizacion-test', controladorCotizacion.test);
router.get('/obtener-estados', controladorCotizacion.obtenerEstados);
router.post('/obtener-codigo-postal', controladorCotizacion.obtenerCodigoPostal);
router.get('/obtener-tipos-de-envios', controladorCotizacion.obtenerTiposDeEnvios);
router.get('/obtener-tipos-de-entregas', controladorCotizacion.obtenerTiposDeEntregas);
router.get('/obtener-servicios-rapidos', controladorCotizacion.obtenerServiciosRapidos);
router.post('/cotizar', controladorCotizacion.cotizar);
router.post('/predocumentar', controladorCotizacion.predocumentar);

router.get('/cotizacion', (request, response) => {
    response.render('cotizacion');
});


// rutas nuevas jose
router.post('/alta-clientes', controladorCotizacion.altaClientes);
router.post('/login', controladorCotizacion.ejecutaLoginService);
router.post('/ejecutar-cargo', controladorCotizacion.ejecutaServicioCargo);
router.post('/ejecutar-historial', controladorCotizacion.ejecutaHistorialCliente);

module.exports = router;