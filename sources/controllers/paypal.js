'use strict'

const axios = require('axios');
var paypal = require('paypal-rest-sdk');
const config = require('../../config');
const Pago = require('../models/pago');

paypal.configure(config.PAYPAL_CONFIG);

var controller = {

    // METODO PARA HACER LA CRACION DE LA COMPRA EN PAYPAL
    crearCompraPaypal: (request, response) => {
        let monto = request.body.monto;
        console.log(monto);
        var create_payment_json = {
            "intent": "authorize",
            "payer": {
                "payment_method": "paypal"
            },
            "redirect_urls": {
                "return_url": "http://127.0.0.1:3000/procesando-paypal",
                "cancel_url": "http://127.0.0.1:3000/compra-paypal-error"
            },
            "transactions": [{
                "amount": {
                    "total": `${monto}.00`,
                    "currency": "USD"
                },
                "description": " a book on mean stack "
            }]
        };

        paypal.payment.create(create_payment_json, function(error, payment){
            if(error){
                console.error(error);
            } else {
                //capture HATEOAS links
                var links = {};
                payment.links.forEach(function(linkObj){
                    links[linkObj.rel] = {
                        'href': linkObj.href,
                        'method': linkObj.method
                    };
                })
            
                //if redirect url present, redirect user
                if (links.hasOwnProperty('approval_url')){
                    response.redirect(links['approval_url'].href);
                } else {
                    console.error('no redirect URI present');
                }
            }
        });
    },

    // METODO PARA PROCESAR LA COMPRA EN PAYPAL
    concretarCompra: (request, response) => {
        var paymentId = request.query.paymentId;
        var payerId = { 'payer_id': request.query.PayerID };
    
        paypal.payment.execute(paymentId, payerId, async function(error, payment){
            if(error){
                console.error(error);
            } else {
                if (payment.state == 'approved'){ 
                    console.log('payment', payment);
                    console.log('payment.transactions[0]', payment.transactions[0]);
                    console.log('payment.transactions[0].related_resources[0]', payment.transactions[0].related_resources[0]);
                    var pago = new Pago();
                    pago.fechaDelPago = Date(payment.update_time);
                    pago.monto = payment.transactions[0].amount.total;
                    pago.banco = 'Pago electrónico';
                    pago.formaDePago = 'PayPal';
                    pago.referencia = payment.id;
                    pago.estatus = 'Procesado';
    
                    const pagoGuardado = await pago.save();

                    // axios.post('http://localhost:5000/respuesta', {
                    //     tipoPago: 'Electronico',
                    //     pasarela: 'PayPal',
                    //     monto: payment.transactions[0].amount.total
                    // })
                    //     .then(function (res) {
                    //         // console.log(response);
                    //         console.log(res.data);
            
                    //         // // LOGICA DE LA RESPUESTA
                    //         // if (res.data.status) {
                    //         //     //registro de pago exitoso
                    //         // } else {
                    //         //     //registro de pago exitoso, mostrar error
                    //         // }
            
                    //         return response.status(200).send(
                    //             res.data
                    //         );
                    //     })
                    //     .catch(function (error) {
                    //         console.log(error);
                    //     });

                    response.redirect('/compra-paypal-exitosa');
                } else {
                    response.redirect('/compra-paypal-error');
                }
            }
        });
    },

};

module.exports = controller;
