'use strict'

const axios = require('axios');

var controller = {

    test: (request, response) => {
        console.log(request.body);
        return response.status(200).send({
            message: 'Pueba controlador de cotizacion'
        });
    },

    // nuevos metodos jose
    altaClientes: (request, response) => {
        console.log(request.body);

        const nombreCliente = request.body.nombre+" "+request.body.apellido;

        axios.post('https://wikienvios.gbts.com.mx/WsDocumentacion/rest/WsAltaClientes/altaClientes/', {
            "nombreCliente" : nombreCliente,
            "calle" : "ESTA ES UNA CALLE",
            "colonia" : "AHORA",
            "codigoPostal" : "11320",
            "telefono" : request.body.telefono,
            "correo" : request.body.correo,
            "rfc" : request.body.rfc,
            "numExterno" : "2",
            "numInterno" : "1",
            "username" : request.body.correo,
            "pwdPrevio" : "",
            "pwdNuevo" : request.body.password,
            "usuario" : 
            {
                "username" : "FNX",
                "pwd" : "Lwy+Euj55UBm1rCaib8xcg=="
            }
        }, {
            headers: { 'Content-Type': 'application/json' }
        })
            .then((res) => {
                console.log(res.data);
                return response.status(200).send({
                    response: res.data
                });
            })
            .catch((error) => {
                console.log(error);
            })
    },

    ejecutaLoginService: (request, response) => {
        console.log(request.body);

        axios.post('https://wikienvios.gbts.com.mx/WsDocumentacion/login/WsLoginService/ejecutaLoginService', {
            "cadenaCliente" : request.body.correo,
            "pwd" : request.body.password,
            "fecha" : new Date()
        }, {
            headers: { 'Content-Type': 'application/json' }
        })
            .then((res) => {
                console.log(res.data);
                return response.status(200).send({
                    response: res.data
                });
            })
            .catch((error) => {
                console.log(error);
            })
    },

    ejecutaServicioCargo: (request, response) => {
        console.log(request.body);

        axios.post('https://wikienvios.gbts.com.mx/WsDocumentacion/cargo/WsServicioCargo/ejecutaServicioCargo', {
            "cadenaCliente" : request.body.cadenaCliente,
            "montoCargo" : request.body.montoCargo,
            "descripcion" : request.body.descripcion,
            "usuario" : 
            {
                "username" : "FNX",
                "pwd" : "Lwy+Euj55UBm1rCaib8xcg=="
            }
        }, {
            headers: { 'Content-Type': 'application/json' }
        })
            .then((res) => {
                console.log(res.data);
                return response.status(200).send({
                    response: res.data
                });
            })
            .catch((error) => {
                console.log(error);
            })
    },

    ejecutaHistorialCliente: (request, response) => {
        console.log(request.body);

        axios.post('https://wikienvios.gbts.com.mx/WsDocumentacion/historial/WsHistorialCliente/ejecutaHistorialCliente/', {
            "cadenaCliente" : request.body.cadenaCliente,
            "usuario" : 
            {
                "username" : "FNX",
                "pwd" : "Lwy+Euj55UBm1rCaib8xcg=="
            }
        }, {
            headers: { 'Content-Type': 'application/json' }
        })
            .then((res) => {
                console.log(res.data);
                return response.status(200).send({
                    response: res.data
                });
            })
            .catch((error) => {
                console.log(error);
            })
    },


    //end  nuevos metodos jose
    
    obtenerEstados: (request, response) => {
        console.log(request.body);
        axios.post('https://wikienvios.gbts.com.mx/WsDocumentacion/rest/WsDocumentacion/busquedaEstado/', {
            "username": "FNX",
            "pwd": "Lwy+Euj55UBm1rCaib8xcg=="
        }, {
            headers: { 'Content-Type': 'application/json' }
        })
            .then((res) => {
                console.log(res.data);
                return response.status(200).send({
                    response: res.data
                });
            })
            .catch((error) => {
                console.log(error);
            })
    },

    obtenerCodigoPostal: (request, response) => {
        console.log(request.body);
        axios.post('https://wikienvios.gbts.com.mx/WsDocumentacion/rest/WsDocumentacion/busquedaCodigoPostal/', {
            "codigoPostal": request.body.codigoPostal,
            "usuario": {
                "username": "FNX",
                "pwd": "Lwy+Euj55UBm1rCaib8xcg=="
            }
        }, {
            headers: { 'Content-Type': 'application/json' }
        })
            .then((res) => {
                console.log(res.data);
                response.setHeader('Content-Type', 'application/json');
                return response.status(200).send(
                   res.data.lista
                ); 

               /*  return response.json({ 
                    results: res.data.lista, 
                }); */
            })
            .catch((error) => {
                console.log(error);
            })
    },

    obtenerTiposDeEnvios: (request, response) => {
        console.log(request.body);
        axios.post('https://wikienvios.gbts.com.mx/WsDocumentacion/rest/WsDocumentacion/busquedaTipoEnvio/', {
            "username": "FNX",
            "pwd": "Lwy+Euj55UBm1rCaib8xcg=="
        }, {
            headers: { 'Content-Type': 'application/json' }
        })
            .then((res) => {
                console.log(res.data);
                return response.status(200).send({
                    response: res.data
                });
            })
            .catch((error) => {
                console.log(error);
            })
    },

    obtenerTiposDeEntregas: (request, response) => {
        console.log(request.body);
        axios.post('https://wikienvios.gbts.com.mx/WsDocumentacion/rest/WsDocumentacion/busquedaTipoEntrega/', {
            "username": "FNX",
            "pwd": "Lwy+Euj55UBm1rCaib8xcg=="
        }, {
            headers: { 'Content-Type': 'application/json' }
        })
            .then((res) => {
                console.log(res.data);
                return response.status(200).send({
                    response: res.data
                });
            })
            .catch((error) => {
                console.log(error);
            })
    },

    obtenerServiciosRapidos: (request, response) => {
        console.log(request.body);
        axios.post('https://wikienvios.gbts.com.mx/WsDocumentacion/rest/WsDocumentacion/busquedaServiciosRapidos/', {
            "username": "FNX",
            "pwd": "Lwy+Euj55UBm1rCaib8xcg=="
        }, {
            headers: { 'Content-Type': 'application/json' }
        })
            .then((res) => {
                console.log(res.data);
                return response.status(200).send({
                    response: res.data
                });
            })
            .catch((error) => {
                console.log(error);
            })
    },

    cotizar: (request, response) => {
        //console.log(request.body);
        //let paquetes = [];
        //paquetes.push(JSON.parse(request.body.listaPaquetes));

        /* request.body.listaPaquetes.forEach(element => {
            paquetes.push(JSON.parse(element));
        }); */

        //console.log(paquetes);
        //request.body.listaPaquetes = paquetes;
        //console.log(request.body); 


        axios.post('https://wikienvios.gbts.com.mx/WsDocumentacion/rest/WsDocumentacion/cotizacion/', {
            "codigoPostalOrigen": request.body.codigoPostalOrigen,
            "codigoPostalDestino": request.body.codigoPostalDestino,
            "idTipoEnvio": request.body.idTipoEnvio,
            "idTipoEntrega": "2",
            "idTarifa": "null",
            "numeroCliente": "5639",
            "requiereSeguro": "false",
            "valorDeclarado": "null",
            "listaPaquetes": request.body.listaPaquetes,
            "usuario":
            {
                "username": "FNX",
                "pwd": "Lwy+Euj55UBm1rCaib8xcg=="
            }
        }, {
            headers: { 'Content-Type': 'application/json' }
        })
            .then((res) => {
                console.log(res.data);
                return response.status(200).send({
                    response: res.data
                });
            })
            .catch((error) => {
                console.log(error);
            }) 
    },

    predocumentar: (request, response) => {

        console.log(request.body);

        axios.post('https://wikienvios.gbts.com.mx/WsDocumentacion/rest/WsDocumentacion/predocumentacion/', 
        {
            "numeroGuia": "",
            "username": "FNX",
            "codigoPostalOrigen": request.body.idCodigoPostalOrigen,
            "codigoPostalDestino": request.body.idCodigoPostalDestino,
            "idTipoEnvio": request.body.idTipoEnvio,
            "idTipoEntrega": "2",
            "idServicioRapido": "0",
            "idTarifa": "null",
            "numeroCliente": "5639",
            "contenido": "Un contenido",
            "observaciones": "Unas observaciones",
            "requiereSeguro": "false",
            "valorDeclarado": "null",
            "remitente":
            {
                "idCodigoPostal": request.body.idCodigoPostalOrigen,
                "numeroCliente": "5639",
                "nombre": request.body.nombreOrigen,
                "direccion": request.body.direccionOrigen,
                "telefono": request.body.telefonoOrigen,
                "contacto": request.body.nombreOrigen,
                "correo": request.body.emailOrigen,
            },
            "destinatario":
            {
                "idCodigoPostal": request.body.idCodigoPostalDestino,
                "numeroCliente": "5639",
                "nombre": request.body.nombreDestinatario,
                "direccion": request.body.direccionDestinatario,
                "telefono": request.body.telefonoDestinatario,
                "contacto": request.body.nombreDestinatario,
                "correo": request.body.emailDestinatario,
            },
            "listaPaquetes":
                [
                    {
                        "cantidad": request.body.cantidad,
                        "alto": request.body.alto,
                        "ancho": request.body.ancho,
                        "largo": request.body.largo,
                        "peso": request.body.peso
                    },
                    /*{
                        "cantidad": "1",
                        "alto": "15",
                        "ancho": "2",
                        "largo": "22",
                        "peso": "15"
                    }*/
                ],
            "usuario":
            {
                "username": "FNX",
                "pwd": "Lwy+Euj55UBm1rCaib8xcg=="
            }
        }
        ,{
            headers: { 'Content-Type': 'application/json' }
        })
        .then((res) => {
                console.log(res.data);
                return response.status(200).send({
                    response: res.data
                });
            })
            .catch((error) => {
                return response.status(400).send({
                    error
                });
            })
        },
};

module.exports = controller;