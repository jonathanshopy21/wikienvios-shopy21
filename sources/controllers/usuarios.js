'use strict'

const axios = require('axios');

var controller = {

    test: (request, response) => {
        console.log(request.body);
        return response.status(200).send({
            message: 'Pueba controlador de usuarios'
        });
    },

    guardarUsuario: (request, response) => {
        console.log(request.body);
        const parameters = request.body;
        const nombre = parameters.nombre;
        const apellido = parameters.apellido;
        const correo = parameters.correo;
        const telefono = parameters.telefono;
        const contrasena = parameters.contrasena;

        axios.post('http://localhost:5000/respuesta', {
            nombre: nombre,
            apellido: apellido,
            correo: correo,
            telefono: telefono,
            contrasena: contrasena,
        })
            .then(function (res) {
                // console.log(response);
                console.log(res.data);

                // // LOGICA DE LA RESPUESTA
                // if (res.data.status) {
                //     //registro exitoso, enviar directo a panel
                //     return response.status(200).redirect('PANEL');
                // } else {
                //     //registro no exitos, enviar a pagina de login y mostrar mensaje de error
                //     return response.status(200).redirect('LOGIN');
                // }

                return response.status(200).send(
                    res.data
                );
            })
            .catch(function (error) {
                console.log(error);
            });
    },

    inicioDeSesion: (request, response) => {
        console.log(request.body);
        const parameters = request.body;
        const correo = parameters.correo;
        const contrasena = parameters.contrasena;

        axios.post('http://localhost:5000/respuesta', {
            correo: correo,
            contrasena: contrasena,
        })
            .then(function (res) {
                // console.log(response);
                console.log(res.data);

                // // LOGICA DE LA RESPUESTA
                // if (res.data.status) {
                //     //inicio de sesion exitoso, enviar directo a panel
                //     return response.status(200).redirect('PANEL');
                // } else {
                //     //inicio de sesion no exitos, enviar a pagina de login y mostrar mensaje de error
                //     return response.status(200).redirect('LOGIN');
                // }

                return response.status(200).send(
                    res.data
                );
            })
            .catch(function (error) {
                console.log(error);
            });
    },
};

module.exports = controller;