'use strict'

const config = require('../../config');
const stripe = require('stripe')(config.STRIPE_SECRET);
const Pago = require('../models/pago');

var controller = {

    // CREACION Y PROCESADO DEL PAGO STRIPE
    comprarStripe: (request, response) => {
        try {
            stripe.customers.create({
                name: request.body.name,
                email: request.body.email,
                source: request.body.stripeToken
            }).then(customer => stripe.charges.create({
                amount: request.body.amount * 100,
                currency: 'usd',
                customer: customer.id,
                description: 'Pago a Wikienvíos.'
            })).then(async (charge) => {
                console.log('charge', charge);
                var pago = new Pago();
                pago.fechaDelPago = Date(charge.created);
                pago.monto = charge.amount / 100;
                pago.banco = 'Pago electrónico';
                pago.formaDePago = 'Stripe';
                pago.referencia = charge.id;
                pago.estatus = 'Procesado';

                const pagoGuardado = await pago.save();

                // axios.post('http://localhost:5000/respuesta', {
                //     tipoPago: 'Electronico',
                //     pasarela: 'Stripe',
                //     monto: charge.amount / 100
                // })
                //     .then(function (res) {
                //         // console.log(response);
                //         console.log(res.data);

                //         // // LOGICA DE LA RESPUESTA
                //         // if (res.data.status) {
                //         //     //registro de pago exitoso
                //         // } else {
                //         //     //registro de pago exitoso, mostrar error
                //         // }

                //         return response.status(200).send(
                //             res.data
                //         );
                //     })
                //     .catch(function (error) {
                //         console.log(error);
                //     });

                response.redirect('compra-stripe-exitosa');
            })
                .catch(error => console.log(error))
        } catch (error) { response.send(error) }
    },
};

module.exports = controller;
