'use strict'

const axios = require('axios');

var controller = {

    test: (request, response) => {
        console.log(request.body);
        return response.status(200).send({
            message: 'Test de prueba para la estructura del APIRest'
        });
    },
};

module.exports = controller;