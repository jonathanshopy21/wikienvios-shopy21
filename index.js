'use strict'

var mongoose = require('mongoose');
var app = require('./app')
var port = 3000;

mongoose.Promise = global.Promise;

// Conexion a la base de datos
mongoose.connect('mongodb://localhost:27017/wikienvios', { useNewUrlParser: true }).then(() => {
    console.log('Connection to the database established successfully...');

    // Creacion del servidor
    app.listen(port, () => {
        console.log(`Server running correctly in the url: localhost: ${port}`);
    });
}).catch((error) => {
    console.log(error);
});
