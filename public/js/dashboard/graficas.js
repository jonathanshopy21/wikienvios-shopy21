
// grafica de enviados
var ctx = document.getElementById('enviados');
var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: ['Pendientes', 'Completados'],
        datasets: [{
            label: '# of Votes',
            data: [0, 100],
            backgroundColor: [
                '#CECDCD',
                '#00BDB7'
                
            ],
            /* borderColor: [
                '#CECDCD)',
                '#00BDB7',
            ],
            borderWidth: 1 */
        }]
    },
    options: {
        legend: {
            display: false
        },
        title: {
        display: true,
        text: 'ENVIADOS', 
        position: 'bottom'
    }
}
    
});


// grafica de en transito
var ctx = document.getElementById('en-transito');
var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: ['Pendientes', 'Completados'],
        datasets: [{
            label: '# of Votes',
            data: [50, 50],
            backgroundColor: [
                '#CECDCD',
                '#00BDB7'
                
            ],
            /* borderColor: [
                '#CECDCD)',
                '#00BDB7',
            ],
            borderWidth: 1 */
        }]
    },
    options: {
        legend: {
            display: false
        },
        title: {
        display: true,
        text: 'EN TRANSITO', 
        position: 'bottom'
    }
}
    
});


// grafica de entregado
var ctx = document.getElementById('entregado');
var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: ['Pendientes', 'Completados'],
        datasets: [{
            label: '# of Votes',
            data: [25, 75],
            backgroundColor: [
                '#CECDCD',
                '#00BDB7'
                
            ],
            /* borderColor: [
                '#CECDCD)',
                '#00BDB7',
            ],
            borderWidth: 1 */
        }]
    },
    options: {
        legend: {
            display: false
        },
        title: {
        display: true,
        text: 'ENTREGADO', 
        position: 'bottom'
    }
}
    
});


// grafica de entregado
var ctx = document.getElementById('incidencia');
var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: ['Pendientes', 'Completados'],
        datasets: [{
            label: '# of Votes',
            data: [75, 25],
            backgroundColor: [
                '#CECDCD',
                '#00BDB7'
                
            ],
            /* borderColor: [
                '#CECDCD)',
                '#00BDB7',
            ],
            borderWidth: 1 */
        }]
    },
    options: {
        legend: {
            display: false
        },
        title: {
        display: true,
        text: 'INCIDENCIA', 
        position: 'bottom'
    }
}
    
});    
