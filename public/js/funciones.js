// funcion para validar el correo
function validarCorreo(email){
    var caract = new RegExp(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/);

    if (caract.test(email) == false){
        return false;
    }else{
        return true;
    }
}

// validar campos vacios se le pasa un objeto como parametro
function validarVacio(obj) {
    let output = true;
    $.each(obj, function (key, item) { 
        if (item.length <= 0 || item <= 0) {
            output = false;
        } 
    }); 

    return output;
}

// formatear una cantidad con javascript
function formatNumber(num) {
    if (!num || num == 'NaN') return '-';
    if (num == 'Infinity') return '&#x221e;';
    num = num.toString().replace(/\$|\,/g, '');
    if (isNaN(num))
        num = "0";
    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num * 100 + 0.50000000001);
    cents = num % 100;
    num = Math.floor(num / 100).toString();
    if (cents < 10)
        cents = "0" + cents;
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3) ; i++)
        num = num.substring(0, num.length - (4 * i + 3)) + '.' + num.substring(num.length - (4 * i + 3));
    return (((sign) ? '' : '-') + num + ',' + cents);
}

function removerPaquete(id){

    datosPaquete.splice(id);
    $("#pk"+id).remove();
    console.log(datosPaquete);
}