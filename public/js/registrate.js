var errorPass = $('#errorPass').hide();

// objeto con los datos enviados del formulario
$("#btnGuardar").click(function() {
    var obj = {
        'name': $("#nombre").val(),
        'lastname': $("#apellido").val(),
        'email': $("#mail").val(),
        'phoneNumber': $("#telefono").val(),
        'RFC': $("#rfc").val(),
        'password': $("#pass").val()
    };

    // mostrar mensaje si la contrasena no coincide
    if (obj.password != $("#replyPass").val()) {
        errorPass.show();
    }


    console.log(obj);
})