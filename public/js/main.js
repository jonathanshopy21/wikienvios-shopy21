// variables globales
var loader = $("#spinner");

// => login <=
$("#login").click(function(){
    let correo = $("#email").val();
    let password = $("#password").val();

    $.ajax({
        type : 'post',
        url : 'http://localhost:3000/login',
        dataType : "json",
        beforeSend: function () {
        // mostrar algo mientras espera la peticion ajax
            loader.fadeIn();  
        },
        data:{
            'correo': correo,
            'password': password
        },
        success:function(request){   
          
         // mostrar en consola
           console.log(request.response);

           if (request.response.error == true) {
               $("#error-login").html(request.response.mensaje);
           } else {
                window.location.href = "/dashboard";
           }

           // ocultar spinner de carga
           loader.fadeOut(1000); 
        }
    });
    
})

// => registro de usuarios <=
$("#btnGuardar").click(function() {
    let obj = {
        'nombre': $("#nombre").val(),
        'apellido': $("#apellido").val(),
        'correo': $("#correo").val(),
        'telefono': $("#telefono").val(),
        'rfc': $("#rfc").val(),
        'password': $("#passwordRegister").val()        
    };

    let error = $("#errorRegister");
   
    // mostrar mensaje si la contrasena no coincide
    if (validarCorreo(obj.correo) === false) {
        error.html('Correo no valido!');
    } else if (obj.password != $("#replyPass").val()) {
        error.html('Las contraseñas no coinciden por favor verifica...');
    } else if(validarVacio(obj) == false){
        error.html('Por favor rellene todos los campos para continuar!');
    } else {
        // enviar el formulario para ser procesado
        $.ajax({
            type : 'post',
            url : 'http://localhost:3000/alta-clientes',
            dataType : "json",
            beforeSend: function () {
            // mostrar algo mientras espera la peticion ajax
                loader.fadeIn();  
            },
            data:obj,
            success:function(request){   
              
                console.log(request.response);    

                // mostrar en consola
                if (request.response.error == true) {
                    error.html(request.response.mensaje);
                } else {
                    window.location.href = "/dashboard";
                }
                  
                // ocultar spinner de carga
                loader.fadeOut(1000); 
            }
        });
    } 

})
// end registro de usuarios


// => quitar ceros al tener el foco en la cotizacion <=
if ($("#peso").length) {
    
    var cantidad = $("#cantidad");
    var alto = $("#alto");
    var ancho = $("#ancho");
    var largo = $("#largo");
    var peso = $("#peso");
    cantidad.focus(function(){
        cantidad.val('');
    })
    alto.focus(function(){
        alto.val('');
    })
    ancho.focus(function(){
        ancho.val('');
    })
    largo.focus(function(){
        largo.val('');
    })
    peso.focus(function(){
        peso.val('');
    })
}
// end quitar ceros   

// cotizar un solo paquete
var datosPaquete = [];
var dataCotizar;
$("#peso").change(function(){

    if (datosPaquete.length > 0) {
        //console.log('esta lleno')
    } else {
       
        let paquete = {
            'cantidad': cantidad.val(),
            'alto': alto.val(),
            'ancho': ancho.val(),
            'largo': largo.val(),
            'peso': peso.val()
        }; 

        // asignar valores de el tipo de envio
        if ($('#Check1').prop("checked") == true) {
            // paquete
            var tipoEnvio = 2;
        } else if ($('#Check2').prop("checked") == true) {
            // doumento o sobre
            var tipoEnvio = 1;
        } 

        // objeto para validar que los campos fueron rellenados
            let obj = {
                'codigoPostalOrigen': $("#codigoPostalOrigen").val(),
                'codigoPostalDestino': $("#codigoPostalDestino").val(),
                'idTipoEnvio': tipoEnvio            
            }   

        if (validarVacio(paquete) == true || validarVacio(obj) == true) {

            let error = $("#errorCotizacion");

            // data a enviar por ajax
            dataCotizar = {
                'codigoPostalOrigen': $("#codigoPostalOrigen").val(),
                'codigoPostalDestino': $("#codigoPostalDestino").val(),
                'idTipoEnvio': tipoEnvio,
                'listaPaquetes': [paquete]
            };

            //console.log(dataCotizarPaquete);
            $.ajax({
                type : 'post',
                url : 'http://localhost:3000/cotizar',
                contentType: 'application/json',
                beforeSend: function () {
                // mostrar algo mientras espera la peticion ajax
                    loader.fadeIn();  
                },
                data:JSON.stringify(dataCotizar),
                success:function(request){   
                
                    //console.log(request.response);    
                    
                    // formatear numeros
                    let subtotal = formatNumber(request.response.subTotalGeneral);
                    let total = formatNumber(request.response.totalGeneral);

                    // mostrar mensaje de error si algo falla
                    if (request.response.error == true) {
                        //error.fadeIn().html(request.response.mensaje);
                    } else {
                        error.fadeOut();
                                                        
                        // pintar en pantalla subtotal y total de la cotizacion
                        $("#subtotalCotizacion").html(subtotal+" <span>MX</span>");
                        $("#totalCotizacion").html(total+" <span class='moneda-footer'>MXN</span>");
                    
                    }

                    // ocultar spinner de carga
                    loader.fadeOut(1000); 
                }
            });  
    
        }
    }    
})


// => cotizar varios paquetes <= 
$("#agregarPaqueteCotizacion").click(function() {
    
    let error = $("#errorCotizacion");

    // asignar valores de el tipo de envio
    if ($('#Check1').prop("checked") == true) {
        // paquete
        var tipoEnvio = 2;
    } else if ($('#Check2').prop("checked") == true) {
        // doumento o sobre
        var tipoEnvio = 1;
    } 
    
    // validar si selecciono un tipo de envio
    if($('#Check1').prop("checked") == false && $('#Check2').prop("checked") == false){
        error.fadeIn().html('Por favor seleccione un tipo de envio...');
    } else {

        // añadir datos del paquete al objeto
        let paquete = {
            'cantidad': cantidad.val(),
            'alto': alto.val(),
            'ancho': ancho.val(),
            'largo': largo.val(),
            'peso': peso.val()
        }; 
               

        //console.log(datosPaquete);
        // data a enviar por ajax
        dataCotizar = {
            'codigoPostalOrigen': $("#codigoPostalOrigen").val(),
            'codigoPostalDestino': $("#codigoPostalDestino").val(),
            'idTipoEnvio': tipoEnvio,
            'listaPaquetes': datosPaquete
        };

        // objeto para validar que los campos fueron rellenados
        let obj = {
            'codigoPostalOrigen': $("#codigoPostalOrigen").val(),
            'codigoPostalDestino': $("#codigoPostalDestino").val(),
            'idTipoEnvio': tipoEnvio            
        }   

        //console.log(dataCotizar);
            
        if (validarVacio(paquete) == false ||  validarVacio(obj) == false) {       
            error.fadeIn().html('Por favor rellene todos los campos para continuar!');
        } else {
            // si todo esta ok enviar formulario 
            error.fadeOut();               
            datosPaquete.push(paquete); 

            $.ajax({
                type : 'post',
                url : 'http://localhost:3000/cotizar',
                contentType: 'application/json',
                beforeSend: function () {
                // mostrar algo mientras espera la peticion ajax
                    loader.fadeIn();  
                },
                data:JSON.stringify(dataCotizar),
                success:function(request){   
                  
                    //console.log(request.response);    
                    
                    // formatear numeros
                    let subtotal = formatNumber(request.response.subTotalGeneral);
                    let total = formatNumber(request.response.totalGeneral);

                    // mostrar mensaje de error si algo falla
                    if (request.response.error == true) {
                        error.fadeIn().html(request.response.mensaje);
                    } else {
                        error.fadeOut()
                    
                        // tabla de paquetes agregados
                        let listado = "<table class='table table-responsive'>"+
                        "<caption>Lista de paquetes agregados</caption>"+
                            "<thead>"+
                                "<tr>"+
                                    "<th>Numero de piezas</th>"+
                                    "<th>Largo</th>"+
                                    "<th>Ancho</th>"+
                                    "<th>Alto</th>"+
                                    "<th>Peso</th>"+
                                    "<th>Operaciones</th>"+                    
                                "</tr>"+
                            "</thead>"+
                            "<tbody>";

                            console.log(datosPaquete);

                        let count = 0;   
                        $.each(datosPaquete, function(index, value) {
                            listado +="<tr id='pk"+count+"'>"+
                                        "<td>"+value.cantidad+"</td>"+
                                        "<td>"+value.largo+"</td>"+
                                        "<td>"+value.ancho+"</td>"+
                                        "<td>"+value.alto+"</td>"+
                                        "<td>"+value.peso+"</td>"+ 
                                        "<td><img src='/img/web_elements/delete-package.svg' onclick='removerPaquete("+count+")'></td>"+                        
                                    "</tr>";      
                                    
                            count++;       
                        });

                        listado += "</tbody>"+
                        "</table>";

                        // pintar en pantalla el listado con el objeto
                        $("#lista-paquetes").html(listado);

                        // pintar en pantalla subtotal y total de la cotizacion
                        $("#subtotalCotizacion").html(subtotal+" <span>MX</span>");
                        $("#totalCotizacion").html(total+" <span class='moneda-footer'>MXN</span>");

                        // limpiar campos
                        cantidad.val(0);
                        largo.val(0);
                        ancho.val(0);
                        alto.val(0);
                        peso.val(0);
                    }

                    // ocultar spinner de carga
                    loader.fadeOut(1000); 
                }
            });  
        }         
    }    
})


// enviar variable con la data a la vista envios
$("#btnCotizacion").click(function(){
        
    if (dataCotizar != undefined) {
        // Guardo el objeto como un string en el localstore
        localStorage.setItem('datosCotizacionParse', JSON.stringify(dataCotizar));
        window.location.href = "/dashboard/envios";
    }
          
}) 

// barrer informacion de paquetes
$("#barrer").click(function(){    
    $("#errorCotizacion").fadeOut();
    cantidad.val(0);
    largo.val(0);
    ancho.val(0);
    alto.val(0);
    peso.val(0);    

    //console.log(datosPaquete);
});


 
// traer historial de pagos
if ($('#historial-credito').length) {

    $.ajax({
        type : 'post',
        url : 'http://localhost:3000/ejecutar-historial',
        dataType: 'json',
        beforeSend: function () {
        // mostrar algo mientras espera la peticion ajax
            loader.fadeIn();  
        },
        data:{'cadenaCliente': 'jonathan@shopy21.com'},
        success:function(request){   
          
            let response = request.response.historialCliente.listaHistorialCliente;
            
            let listado = "<table class='table-responsive labels-table-rastreo historial-credito'>"+
                                "<thead>"+
                                    "<tr>"+
                                        "<th><div>FECHA</div></th>"+
                                        "<th><div class='descript'>DESCRIPCIÓN</div></th>"+
                                        "<th><div>MONTO</div></th>"+
                                        "<th><div>SALDO</div></th>"+
                                    "</tr>"+                                                    
                                "</thead>"+
                            "<tbody style='border: 2px solid #fed55d;' id='historial-credito'>";
            $.each(response, function(index, value) {
                listado += "<tr>"+
                    "<td>15 / 12 / 2019</td>"+
                    "<td class='text-left'>ABONO</td>"+
                    "<td></td>"+
                    "<td class='monto-verde'>$ 2.000,000.00</td>"+
                "</tr>";           
            });

            listado += "</tbody></table>";

            console.log(response);    
                         
            // ocultar spinner de carga
            loader.fadeOut(1000); 
        }
    }); 
}


// => modal de pagos <=
    // seleccion para pagar con stripe
    $( "#stripe" ).click(function() {

        // ocultar formularios si hay alguno abierto
        $("#caja-spei").fadeOut(0);
        $("#caja-transferencia").fadeOut(0);
        $("#caja-deposito").fadeOut(0);
        $("#caja-practicaja").fadeOut(0);

        //aparecer formulario
        $("#caja-stripe").fadeIn(1000);

        // opacity a las imagenes de las opciones
        $("#paypal").css({'opacity': .2});
        $("#stripe").css({'opacity': 1});
        $("#spei").css({'opacity': .2});
        $("#transferencia").css({'opacity': .2});
        $("#deposito").css({'opacity': .2});
        $("#practicaja").css({'opacity': .2});
    });

    // seleccion para pagar con spei
    $( "#spei" ).click(function() {

        // ocultar formularios si hay alguno abierto
        $("#caja-stripe").fadeOut(0);
        $("#caja-transferencia").fadeOut(0);
        $("#caja-deposito").fadeOut(0);
        $("#caja-practicaja").fadeOut(0);

        //aparecer formulario
        $("#caja-spei").fadeIn(1000);

        // opacity a las imagenes de las opciones
        $("#paypal").css({'opacity': .2});
        $("#stripe").css({'opacity': .2});
        $("#spei").css({'opacity': 1});
        $("#transferencia").css({'opacity': .2});
        $("#deposito").css({'opacity': .2});
        $("#practicaja").css({'opacity': .2});
    });

    // seleccion para pagar con transferencia
    $( "#transferencia" ).click(function() {

        // ocultar formularios si hay alguno abierto
        $("#caja-stripe").fadeOut(0);
        $("#caja-spei").fadeOut(0);
        $("#caja-deposito").fadeOut(0);
        $("#caja-practicaja").fadeOut(0);

        //aparecer formulario
        $("#caja-transferencia").fadeIn(1000);

        // opacity a las imagenes de las opciones
        $("#paypal").css({'opacity': .2});
        $("#stripe").css({'opacity': .2});
        $("#spei").css({'opacity': .2});
        $("#transferencia").css({'opacity': 1});
        $("#deposito").css({'opacity': .2});
        $("#practicaja").css({'opacity': .2});
    });

    // seleccion para pagar con deposito en efectivo
    $( "#deposito" ).click(function() {

        // ocultar formularios si hay alguno abierto
        $("#caja-stripe").fadeOut(0);
        $("#caja-spei").fadeOut(0);
        $("#caja-transferencia").fadeOut(0);
        $("#caja-practicaja").fadeOut(0);

        //aparecer formulario
        $("#caja-deposito").fadeIn(1000);

        // opacity a las imagenes de las opciones
        $("#paypal").css({'opacity': .2});
        $("#stripe").css({'opacity': .2});
        $("#spei").css({'opacity': .2});
        $("#transferencia").css({'opacity': .2});
        $("#deposito").css({'opacity': 1});
        $("#practicaja").css({'opacity': .2});
    });


    // seleccion para pagar con practicaja
    $( "#practicaja" ).click(function() {

        // ocultar formularios si hay alguno abierto
        $("#caja-stripe").fadeOut(0);
        $("#caja-spei").fadeOut(0);
        $("#caja-transferencia").fadeOut(0);
        $("#caja-deposito").fadeOut(0);

        //aparecer formulario
        $("#caja-practicaja").fadeIn(1000);

        // opacity a las imagenes de las opciones
        $("#paypal").css({'opacity': .2});
        $("#stripe").css({'opacity': .2});
        $("#spei").css({'opacity': .2});
        $("#transferencia").css({'opacity': .2});
        $("#deposito").css({'opacity': .2});
        $("#practicaja").css({'opacity': 1});
    });


// => logica de envios <=    
if ($("#codigoPostalOrigenEnvios").length) {
    var datosCotizacionParse = JSON.parse(localStorage.getItem("datosCotizacionParse"));
    console.log(datosCotizacionParse);

    // colocar valores enviados desde la cotizacion
    $("#codigoPostalOrigenEnvios").val(datosCotizacionParse.codigoPostalOrigen);
    $("#codigoPostalDestinoEnvios").val(datosCotizacionParse.codigoPostalDestino);
}
